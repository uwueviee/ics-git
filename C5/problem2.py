# Alcide Viau
from os import system, name 
from time import sleep 
import random
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear') 
clear()
correctNumber = random.randint(1,50)
while 1:
    print("I'm thinking of a number between 1 and 50, what is it?")
    userNumber = input("> ")
    if userNumber == correctNumber:
        print("You are correct!")
        exit()
    if userNumber > correctNumber:
        print("Lower")
    if userNumber < correctNumber:
        print("Higher")