# Alcide Viau
# -*- coding: utf-8 -*-
from os import system, name 
from time import sleep 
import random
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()

cards = ["King","Queen","Jack","10","9","8","7","6","5","4","3","2","Ace"]
userScore = 0
dealerScore = 0
while 1:
    userCard = random.randint(0,12)
    dealerCard = random.randint(0,12)
    print "User Score:",userScore
    print "Dealer Score:",dealerScore
    print "================"
    print "Your Card: ",cards[userCard]
    print "Dealer Card:",cards[dealerCard]
    if dealerCard > userCard:
        print "Dealer wins"
        dealerScore=dealerScore+1
        sleep(2)
        clear()
    elif userCard > dealerCard:
        print "User wins"
        userScore=userScore+1
        sleep(2)
        clear()
    elif userCard == dealerCard:
        print "Tie"
        sleep(2)
        clear()
    else:
        exit()