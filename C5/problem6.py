# Alcide Viau
from os import system, name 
from time import sleep 
import random
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
userScore = 0
dealerScore = 0
cards = ["King","Queen","Jack","10","9","8","7","6","5","4","3","2","Ace"]
suits = ["Clubs","Diamonds","Hearts","Spades"]
while 1:
    userCard = random.randint(0,12)
    dealerCard = random.randint(0,12)
    typeOfCardUser = random.randint(0,3)
    typeOfCardDealer = random.randint(0,3)
    print "User Score:",userScore
    print "Dealer Score:",dealerScore
    print "================"
    print "Your Card: ",cards[userCard],suits[typeOfCardUser]
    print "Dealer Card:",cards[dealerCard],suits[typeOfCardDealer]
    
    if dealerCard > userCard:
        print "Dealer wins"
        dealerScore=dealerScore+1
        sleep(2)
        clear()
    elif userCard > dealerCard:
        print "User wins"
        userScore=userScore+1
        sleep(2)
        clear()
    elif userCard == dealerCard:
        print "Tie"
        sleep(2)
        clear()
    else:
        exit()