# Alcide Viau
# -*- coding: utf-8 -*-
from os import system, name 
from time import sleep 
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
preTemp = 1
while preTemp != 0:
    preTemp = input("Enter temperature in Celcius > ")
    aftTemp = (preTemp*1.8)+32
    print int(round(aftTemp)),"°F"
    if preTemp > 10:
        print("That's lukewarm")
    elif preTemp > 29:
        print("That's warm")
    elif preTemp < 20:
        print("That's cool")
