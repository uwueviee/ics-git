# Alcide Viau
# -*- coding: utf-8 -*-
from os import system, name 
from time import sleep 
import random
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')

playerOne = ""
playerTwo = ""
playerOneCards = []
playerTwoCards = []
typeOfError = ""
errInput = ""
def mainMenu():
    clear()
    print("  NOU!N   NOU!NO   OU!NOU       U!NOU!  U!NOU!  U!NO  ")
    print("  U!NOU   U!NOU!  U!NOU!NO      NOU!NO  NOU!N   NOU!  ")
    print("   OU!NO   OU!N  !NOU  OU!N      !NO     !NO    U!NO  ")
    print("   !NOU!N  !NO   OU!    NOU       U!     OU!    NOU!  ")
    print("   OU!NOU! OU!  U!NO    U!NO      NO     !NO    U!NO  ")
    print("   !NOU!NO !NO  NOU!    NOU!      U!     OU!    NOU!  ")
    print("   OU! OU!NOU!   !NO    U!N       NOU    !NO    U!N   ")
    print("   !NO !NOU!NO   OU!    NOU       U!N    OU!     OU   ")
    print("   OU!  U!NOU!   !NOU  OU!N       NOU!NOU!N      !N   ")
    print("  U!NOU  OU!NO    U!NOU!NO         !NOU!NO            ")
    print("  NOU!N   NOU!     OU!NOU            !NOU       U!NO  ")
    print("                                                NOU!  ")
    print("")
    print("Main Menu:")
    print("1 - Start Game")
    print("2 - Help")
    print("3 - Quit")
    menuInput = input("> ")
    if menuInput == 1:
        setupGame()
    elif menuInput == 2:
        err("unkownCommand", "help")
    elif menuInput == 3:
        exit()
    elif menuInput == 21:
        clear()
        print("===================== NOU! =====================")
        print("  ++++ System Message ++++")
        print("  +     Developer Mode   +")
        print("  +       Activated      +")
        print("  ++++++++++++++++++++++++")
        print("")
        print("")
        print("NOU! Console")
        while 1:
            developerModeInput = raw_input("> ")
            if developerModeInput == "about" or developerModeInput == "about\r":
                print("NOU! Version 2018.2.10")
                print("NOU! Console Version 2018.2.10")
            elif developerModeInput == "goto" or developerModeInput == "goto\r":
                print("Where would you like to go?")
                gotoLocation = raw_input()
                print("What are your arguments?")
                gotoArguments = raw_input()
                gotoLocation(gotoArguments)
            elif developerModeInput == "listvariables" or developerModeInput == "listvariables\r":
                print(playerOne, playerTwo, playerOneCards, playerTwoCards, typeOfError, errInput)
            elif developerModeInput == "exit" or developerModeInput == "exit\r":
                exit()
            else:
                print("Command Not Executable")
    else:
        mainMenu()

def setupGame():
    clear()
    print("== NOU! ==")
    print("NOU! is basically a clone of UNO! but without skips or reversal cards.")
    print("Every time a new turn occurs, it's clears the screen so the other player cannot see the player's card.")
    print("You must type NOU in all caps when you have one last playable card or you will gain another card.")
    print("==========")
    print("Please type player one's name")
    playerOne = raw_input("> ")
    print("Please type player two's name")
    playerTwo = raw_input("> ")
    countDownGame(playerOne, playerTwo)

def countDownGame(playerOne, playerTwo):
    clear()
    print("================================")
    print("              NOU!              ")
    print('THE TWO PLAYER "PARTY" CARD GAME')
    print("================================")
    print playerOne
    print("               VS               ") 
    print playerTwo
    print("")
    print("              #####             ")
    print("              # 3 #             ")
    print("              #####             ")
    sleep(1)
    clear()
    print("================================")
    print("              NOU!              ")
    print('THE TWO PLAYER "PARTY" CARD GAME')
    print("================================")
    print playerOne
    print("               VS               ") 
    print playerTwo
    print("")
    print("              #####             ")
    print("              # 2 #             ")
    print("              #####             ")
    sleep(1)
    clear()
    print("================================")
    print("              NOU!              ")
    print('THE TWO PLAYER "PARTY" CARD GAME')
    print("================================")
    print playerOne
    print("               VS               ") 
    print playerTwo
    print("")
    print("              #####             ")
    print("              # 1 #             ")
    print("              #####             ")
    sleep(1)
    clear()
    print("================================")
    print("              NOU!              ")
    print('THE TWO PLAYER "PARTY" CARD GAME')
    print("================================")
    print playerOne
    print("               VS               ") 
    print playerTwo
    print("")
    print("             #######            ")
    print("             # GO! #             ")
    print("             #######            ")
    sleep(1)
    startGame(playerOne, playerTwo)

def startGame(playerOne, playerTwo):
    clear()
    playerOneCards = []
    playerTwoCards = []
    i = 0
    while i <= 5:
        cardToAppend = random.randint(0,2),"|",random.randint(0,11)
        playerOneCards.append(cardToAppend)
        i=i+1
    i = 0
    while i <= 5:
        cardToAppend = random.randint(0,2),"|",random.randint(0,11)
        playerTwoCards.append(cardToAppend)
        i=i+1
    print("===================== NOU! =====================")    
    print("  +++ System Message +++")
    print("  +     Player one     +")
    print("  +     goes first     +")
    print("  ++++++++++++++++++++++")
    sleep(1)
    playerOneTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)

def playerOneTurn(playerOneCards, playerTwoCards, playerOne, playerTwo):
    clear()
    print("===================== NOU! =====================")    
    print("  +++ Player One +++")
    print("")
    print("Your cards:")
    print playerOneCards
    print("")
    print("What would you like to do?")
    playerOneInput = raw_input("> ")
    if playerOneInput == "skip" or playerOneInput == "skip\r":
        playerTwoTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)
    elif playerOneInput == "play" or playerOneInput == "play\r":
        clear()
        print("===================== NOU! =====================")    
        print("  +++ Player One +++")
        print("")
        print("Your cards:")
        print playerOneCards
        print("")    
        input    
        playerTwoTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)
    elif playerOneInput == "21" or playerOneInput == "21\r":
        clear()
        print("===================== NOU! =====================")
        print("  ++++ System Message ++++")
        print("  +     Developer Mode   +")
        print("  +       Activated      +")
        print("  ++++++++++++++++++++++++")
        print("")
        print("")
        print("NOU! Console")
        while 1:
            developerModeInput = raw_input("> ")
            if developerModeInput == "about" or developerModeInput == "about\r":
                print("NOU! Version 2018.2.10")
                print("NOU! Console Version 2018.2.10")
            elif developerModeInput == "goto" or developerModeInput == "goto\r":
                print("Where would you like to go?")
                gotoLocation = raw_input()
                print("What are your arguments?")
                gotoArguments = raw_input()
                gotoLocation(gotoArguments)
            elif developerModeInput == "listvariables" or developerModeInput == "listvariables\r":
                print(playerOne, playerTwo, playerOneCards, playerTwoCards, typeOfError, errInput)
            elif developerModeInput == "exit" or developerModeInput == "exit\r":
                exit()
            else:
                print("Command Not Executable")
    else:
        err("unkownCommand", playerOneInput)
    playerTwoTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)

def playerTwoTurn(playerOneCards, playerTwoCards, playerOne, playerTwo):
    clear()
    print("===================== NOU! =====================")    
    print("  +++ Player Two +++") 
    print("")
    print("Your cards:")
    print playerTwoCards
    print("")
    print("What would you like to do?")
    playerTwoInput = raw_input("> ")
    if playerTwoInput == "skip"or playerTwoInput == "skip\r":
        playerOneTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)
    elif playerTwoInput == "play"or playerTwoInput == "play\r":
        playerOneTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)
    elif playerTwoInput == "21" or playerTwoInput == "21\r":
        clear()
        print("===================== NOU! =====================")
        print("  ++++ System Message ++++")
        print("  +     Developer Mode   +")
        print("  +       Activated      +")
        print("  ++++++++++++++++++++++++")
        print("")
        print("")
        print("NOU! Console")
        while 1:
            developerModeInput = raw_input("> ")
            if developerModeInput == "about" or developerModeInput == "about\r":
                print("NOU! Version 2018.2.10")
                print("NOU! Console Version 2018.2.10")
            elif developerModeInput == "goto" or developerModeInput == "goto\r":
                print("Where would you like to go?")
                gotoLocation = raw_input()
                print("What are your arguments?")
                gotoArguments = raw_input()
                gotoLocation(gotoArguments)
            elif developerModeInput == "listvariables" or developerModeInput == "listvariables\r":
                print(playerOne, playerTwo, playerOneCards, playerTwoCards, typeOfError, errInput)
            elif developerModeInput == "exit" or developerModeInput == "exit\r":
                exit()
            else:
                print("Command Not Executable")
    else:
        err("unkownCommand", playerTwoInput)
    playerOneTurn(playerOneCards, playerTwoCards, playerOne, playerTwo)

def err(typeOfError, errInput):
    clear()
    if typeOfError == "unkownCommand":
        while 1:
            clear()
            print("===================== NOU! =====================")
            print("                                                ")
            print("                     ERROR!                     ")
            print("             Command Not Understood             ")
            print("                                                ")
            print("================================================")
            sleep(1)
            clear()
            print("===================== NOU! =====================")
            print("                                                ")
            print("                 CANNOT CONTINUE!               ")
            print("             Please restart the game            ")
            print("                                                ")
            print("================================================")
            sleep(1)
mainMenu()
