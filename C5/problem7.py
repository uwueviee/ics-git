# Alcide Viau
from os import system, name 
from time import sleep 
import random
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
userScore = 0
dealerScore = 0
cards = ["King","Queen","Jack","10","9","8","7","6","5","4","3","2","Ace"]
suits = ["Clubs","Diamonds","Hearts","Spades"]
while 1:
    userCard = random.randint(0,51)
    dealerCard = random.randint(0,51)
    print userCard
    print dealerCard
    if userCard < 14:
        userCard = userCard/4
    if userCard > 13 and userCard < 27:
        userCard = userCard/4
    if userCard > 26 and userCard < 40:
        userCard = userCard/4
    if userCard > 39:
        userCard = userCard/4
    if dealerCard < 14:
        dealerCard = dealerCard/4
    if dealerCard > 13 and dealerCard < 27:
        dealerCard = dealerCard/4
    if dealerCard > 26 and dealerCard < 40:
        dealerCard = dealerCard/4
    if dealerCard > 39:
        dealerCard = dealerCard/4
    print userCard/4
    print "User Score:",userScore
    print "Dealer Score:",dealerScore
    print "================"
    print "Your Card: ",userCard
    print "Dealer Card:",dealerCard
    if dealerCard > userCard:
        print "Dealer wins"
        dealerScore=dealerScore+1
        sleep(2)
        clear()
    elif userCard > dealerCard:
        print "User wins"
        userScore=userScore+1
        sleep(2)
        clear()
    elif userCard == dealerCard:
        print "Tie"
        sleep(2)
        clear()
    else:
        exit()