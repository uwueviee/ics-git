# Alcide Viau
from os import system, name 
from time import sleep 
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear') 

clear()
userAge = 0
while userAge != -1:
    userAge = input("What is your age? > ")
    if userAge < 3:
        print("You are an infant")
    elif userAge < 13 and userAge > 2:
        print("You are a child")
    elif userAge < 18 and userAge > 13:
        print("You are a teen")
    elif userAge > 17:
        print("You are an adult")
