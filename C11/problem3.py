# Alcide Viau
from os import system, name
import os, json
from time import sleep 
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
studentDatabase = open(os.path.join(__location__, "verysecurestudentdatabasenoteventhensacanhackthis.supersecuredatabase"), "r+")
studentDatabaseAppend = open(os.path.join(__location__, "verysecurestudentdatabasenoteventhensacanhackthis.supersecuredatabase"), "a")
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear') 
def menu():
    clear()
    print "HECKIE SYSTEMS STUDENT ADMINISTRATION"
    print
    print "1. Print All"
    print "2. Add a student"
    print "3. Remove a student"
    print "4. Find a mark"
    print "5. Read data"
    print "6. Print sorted by name"
    print "7. Quit"
    menuInput = input("> ")
    if menuInput == 1:
        print_all()
    if menuInput == 2:
        add_a_student()
    if menuInput == 3:
        remove_a_student()
    if menuInput == 4:
        find_a_mark()
    if menuInput == 5:
        read_data()
    if menuInput == 6:
        print_sorted_as_name()
    if menuInput == 7:
        quit()
def print_all():
    clear()
    studentDatabase.seek(0)
    studentsDic = json.load(studentDatabase)
    studentDatabase.seek(0)
    print studentsDic
    print 
    print "1 - Print again | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        print_all()
    if menuInput == 2:
        menu()       
def add_a_student():
    clear()
    nameOfStudent = raw_input("Enter name of student > ")
    markOfStudent = input("Enter the student's mark > ")
    studentDatabase.seek(0)
    studentsDic = json.load(studentDatabase)
    studentDatabase.seek(0)
    studentsDic[nameOfStudent] = markOfStudent 
    jsonWrite = json.dumps(studentsDic) 
    studentDatabase.seek(0)    
    studentDatabase.write(jsonWrite)
    studentDatabase.flush()
    studentDatabase.seek(0)
    print 
    print "1 - Add another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        add_a_student()
    if menuInput == 2:
        menu()    
def remove_a_student():    
    clear()
    nameOfStudent = raw_input("Enter name of student > ")
    studentDatabase.seek(0)
    studentsDic = json.load(studentDatabase)
    studentDatabase.seek(0)
    studentsDic.pop(nameOfStudent, None)
    jsonWrite = json.dumps(studentsDic) 
    studentDatabase.seek(0)
    studentDatabase.truncate(0)    
    studentDatabase.write(jsonWrite)
    studentDatabase.flush()
    studentDatabase.seek(0)
    print
    print "1 - Remove another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        remove_a_student()
    if menuInput == 2:
        menu()    
def find_a_mark():
    clear()
    markOfStudent = input("Enter the mark > ")
    studentDatabase.seek(0)
    studentsDic = json.load(studentDatabase)      
    for name, mark in studentsDic.items():
        if mark == markOfStudent:
            print name, markOfStudent     
    print
    print "1 - Check another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        find_a_mark()
    if menuInput == 2:
        menu()                     
def read_data():
    clear()
    print "This is unneeded as it is done on the fly and reading the data would do nothing here"   
    sleep(5)
    menu()
def print_sorted_as_name():
    clear()
    studentDatabase.seek(0)
    studentsDic = json.load(studentDatabase)
    studentDatabase.seek(0)    
    for key in sorted(studentsDic.iterkeys()):
        print "%s: %s" % (key, studentsDic[key]) 
    print 
    print "1 - Print again | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        print_sorted_as_name()
    if menuInput == 2:
        menu()                  
menu()