## Alcide Viau
from os import system, name 
from time import sleep
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
characters = 'abcdefghijklmnopqrstuvwxyz'  
userMessage = raw_input("Enter a message to encode here > ")
userMessage = userMessage.strip("")
messageArray = []
encodedMessage = ""
for i in userMessage:
    if i.strip() and i in characters:
        messageArray.append(characters[(characters.index(i) + 5) % 26])   
    else:
        messageArray.append(i)
        encodedMessage = ''.join(messageArray)
clear()
print "Your message is",encodedMessage