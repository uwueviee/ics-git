# Alcide Viau
from os import system, name
import os
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
employees = open(os.path.join(__location__, "employees.txt"), "r")
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
employeesArray = []
for line in employees:
        for words in line.strip().split('/n'):
            employeesArray.append(words)
print employeesArray
britishColumbia = 0
prairies = 0
ontario = 0
quebec = 0
maritimes = 0
for i in employeesArray:
    if i[0] == "1":
        britishColumbia+=1
    elif i[0] == "2":
        prairies+=1
    elif i[0] == "3":
        ontario+=1
    elif i[0] == "4":
        quebec+=1
    elif i[0] == "5":
        maritimes+=1
print
print "Ontario:",ontario
print "Prairies:", prairies
print "British Columbia:",britishColumbia
print "Quebec:",quebec
print "Maritimes:",maritimes
print
if ontario > prairies and ontario > britishColumbia and ontario > quebec and ontario > maritimes:
    print "Ontario has the largest amount of workers."
elif prairies > ontario and prairies > britishColumbia and prairies > quebec and prairies > maritimes:
    print "The Prairies has the largest amount of workers."    
elif britishColumbia > ontario and britishColumbia > prairies and britishColumbia > quebec and britishColumbia > maritimes:
    print "British Columbia has the largest amount of workers."
elif quebec > ontario and quebec > prairies and quebec > britishColumbia and quebec > maritimes:
    print "Quebec has the largest amount of workers."
elif maritimes > ontario and maritimes > prairies and maritimes > britishColumbia and maritimes > quebec:
    print "The Maritimes has the largest amount of workers."    