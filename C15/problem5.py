# Alcide Viau
from os import system, name
import os, json
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
contacts = open(os.path.join(__location__, "contacts.json"), "r+")
contactsAppend = open(os.path.join(__location__, "contacts.json"), "a")
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
def menu():
    clear()
    print
    print "CONTACTS"
    print
    print "1. Add Contact"
    print "2. Show Contact"
    print "3. Delete Contact"
    print "4. Show All Contacts"
    print "5. Quit"
    print
    menuInput = input("> ")
    if menuInput == 1:
        add_contact()
    if menuInput == 2:
        show_contact()
    if menuInput == 3:
        delete_contact()
    if menuInput == 4:
        show_all_contacts()
    if menuInput == 5:
        quit()
def add_contact():
    clear()
    nameOfContact = raw_input("Enter name of contact > ")
    numberOfContact = raw_input("Enter the contact's phone number > ")
    emailOfContact = raw_input("Enter the contact's email > ")
    addressOfContact = raw_input("Enter the contact's home address > ")
    nameOfContact = nameOfContact.strip()
    numberOfContact = numberOfContact.strip()
    emailOfContact = emailOfContact.strip()
    addressOfContact = addressOfContact.strip()
    contacts.seek(0)
    contactsDic = json.load(contacts)
    contacts.seek(0)
    contactsDic[nameOfContact] = numberOfContact, emailOfContact, addressOfContact
    jsonWrite = json.dumps(contactsDic) 
    contacts.seek(0)    
    contacts.write(jsonWrite)
    contacts.flush()
    contacts.seek(0)
    print 
    print "1 - Add another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        add_a_student()
    if menuInput == 2:
        menu()    
def show_contact():
    clear()
    nameOfContact = raw_input("Enter the name > ")
    nameOfContact = nameOfContact.strip()
    contacts.seek(0)
    contactsDic = json.load(contacts)      
    for name, contactInfo in contactsDic.items():
        if nameOfContact == name:
            print "Name:",name  
            print "Phone:",contactInfo[0]
            print "Email:",contactInfo[1]
            print "Address:",contactInfo[2]
    print
    print "1 - Check another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        show_contact()
    if menuInput == 2:
        menu()  
def delete_contact():
    clear()
    nameOfContact = raw_input("Enter name of contact > ")
    nameOfContact = nameOfContact.strip()
    contacts.seek(0)
    contactsDic = json.load(contacts)
    contacts.seek(0)
    contactsDic.pop(nameOfContact, None)
    jsonWrite = json.dumps(contactsDic) 
    contacts.seek(0)
    contacts.truncate(0)    
    contacts.write(jsonWrite)
    contacts.flush()
    contacts.seek(0)
    print
    print "1 - Remove another one | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        remove_a_student()
    if menuInput == 2:
        menu()    
def show_all_contacts():
    clear()
    contacts.seek(0)
    contactsDic = json.load(contacts)
    contacts.seek(0)
    for name, contactInfo in contactsDic.items():
        print "Name:",name  
        print "Phone:",contactInfo[0]
        print "Email:",contactInfo[1]
        print "Address:",contactInfo[2]
        print
    print "1 - Print again | 2 - Go back to menu"
    menuInput = input("> ")
    if menuInput == 1:
        show_all_contacts()
    if menuInput == 2:
        menu()    
menu()