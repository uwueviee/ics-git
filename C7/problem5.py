# Alcide Viau
from os import system, name
import random 
from time import sleep 
def mainMenu(currentScore):
    mathChoiceArray = ["+","-","*","/"]
    print("SUPER ULTRA HARD MATH")
    print "YOUR CURRENT SCORE IS:", currentScore    
    print("1 - Addition | 2 - Subtraction | 3 - Multiplication | 4 - Division | 5 - Quit")
    mathChoice = input("Select a choice > ")
    mathChoiceDisplay = mathChoice-1
    mathChoice = mathChoice-1
    if mathChoice == 4:
        endGame(currentScore)
    else:
        presentQuestion(currentScore,mathChoiceArray,mathChoiceDisplay,mathChoice)    
def randomizeNumbers(numberToGenerate):
    if numberToGenerate == "r1":
        return random.randint(1,9)
    elif numberToGenerate == "r2":
        return random.randint(1,9)
def presentQuestion(currentScore,mathChoiceArray,mathChoiceDisplay,mathChoice):
    clear()
    print("SUPER ULTRA HARD MATH")
    print "YOUR CURRENT SCORE IS:", currentScore
    r1 = randomizeNumbers("r1")
    r2 = randomizeNumbers("r2")
    print "What is",r1,mathChoiceArray[mathChoiceDisplay],r2,"?"
    user = input("> ")
    r3 = 0
    if mathChoice+1 == 1:
        r3 = r1+r2
    elif mathChoice+1 == 2:
        r3 = r1-r2
    elif mathChoice+1 == 3:
        r3 = r1*r2
    elif mathChoice+1 == 4:
        r3 = r1/r2
    if user != r3:
        endGame(currentScore)
    elif user == r3:
        currentScore+=1
        clear()
        mainMenu(currentScore)
def endGame(currentScore):
    clear()
    print("SUPER ULTRA HARD MATH")
    print "YOUR SCORE WAS: ", currentScore
    print "GOODBYE!"
    sleep(2)
    exit()
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear') 
clear()
currentScore = 0
while 1:
    mainMenu(currentScore)