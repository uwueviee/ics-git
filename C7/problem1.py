# Alcide Viau
# -*- coding: utf-8 -*-
from os import system, name 
from time import sleep 
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear')
clear()
def calculateTemp(preTemp):
    aftTemp = (preTemp-32)*5/9
    return int(round(aftTemp))
print "Today we will be learning how to convert farenheit to celcius."
sleep(2)
print "To do this we need to do a really simple math equation, first enter the tempature in celcius below."
userTemp = input("> ")
clear()
print "Excellent, for the first step we will need to remove 32 from",userTemp,"this will be",userTemp-32
sleep(2)
print "Next let's multiply it by 5/9, this will be", calculateTemp(userTemp)
sleep(2)
clear()
print "Now you know how to convert farenheit to celcius."
print "The final tempature is",calculateTemp(userTemp)