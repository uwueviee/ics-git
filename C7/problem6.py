# Alcide Viau
from os import system, name
import random 
from time import sleep 
def clear(): 
    if name == 'nt': 
        _ = system('cls') 
    else: 
        _ = system('clear') 
def deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards):
    dealerCards = []
    playerCards = []
    dealerSuit = 0
    playerSuit = 0
    for x in range(3):
        dealerCardPre = random.randint(0,39)
        if dealerCardPre < 10:
            dealerCard = dealerCardPre/4
            dealerCards.append(dealerCard)
            dealerSuit = 0
        if dealerCardPre > 9 and dealerCardPre < 20:
            dealerCard = dealerCardPre/4
            dealerCards.append(dealerCard)
            dealerSuit = 1
        if dealerCardPre > 19 and dealerCardPre < 30:
            dealerCard = dealerCardPre/4
            dealerCards.append(dealerCard)
            dealerSuit = 2
        if dealerCardPre > 29:
            dealerCard = dealerCardPre/4   
            dealerCards.append(dealerCard)
            dealerSuit = 3
        x=+1    
    for x in range(2):
        userCardPre = random.randint(0,39)
        if userCardPre < 10:
            userCard = userCardPre/4
            playerCards.append(userCard) 
            playerSuit = 0
        if userCardPre > 9 and userCardPre < 20:
            userCard = userCardPre/4
            playerCards.append(userCard) 
            playerSuit = 1
        if userCardPre > 19 and userCardPre < 30:
            userCard = userCardPre/4
            playerCards.append(userCard) 
            playerSuit = 2
        if userCardPre > 29:
            userCard = userCardPre/4 
            playerCards.append(userCard)
            playerSuit = 3  
        x=+1
    if userCardPre==dealerCardPre:
        deal(playerWins,dealerWins,playerDealerTies,cardNames,suits)   
    userStickOrAsk(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards)               
def userStickOrAsk(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards):
    clear()
    print '"PROGRAMMING THIS MADE ME WANT TO CRY" BRAND BLACKJACK'
    print "===================================================="
    print "Player Score:",playerWins
    print "Dealer Score:",dealerWins
    print "Ties:",playerDealerTies
    print "===================================================="
    print "Do you want to stop or do you want to get another card added?"    
    print "1 - Stop | 2 - Another Card | 3 - Quit"
    userMenu = raw_input("> ")
    if userMenu == "1":
        check(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards)
    elif userMenu == "2":
        card1 = random.randint(0,10)
        playerCards.append(card1)
        userStickOrAsk(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards)
    elif userMenu == "3":
        quit()    
    else:
        userStickOrAsk(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards)    
def check(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards):
    totalDealerAmount = 0
    totalPlayerAmount = 0
    for i in dealerCards:
        totalDealerAmount+=i        
    for i in playerCards:
        totalPlayerAmount+=i
    if totalPlayerAmount > 21:
        print "Dealer Wins"
        print "Player Cards:"
        for i in playerCards:
            print cardNames[i], i+2  
        print "Dealer Cards:"    
        for i in dealerCards:
            print cardNames[i], i+2           
        dealerWins+=1
        sleep(2)
        deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,[],[])                    
    if totalDealerAmount==totalPlayerAmount:
        print "Tie"
        print "Player Cards:"
        for i in playerCards:
            print cardNames[i], i+2  
        print "Dealer Cards:"    
        for i in dealerCards:
            print cardNames[i], i+2           
        playerDealerTies+=1
        sleep(2)
        deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,[],[]) 
    if totalDealerAmount > totalPlayerAmount:
        print "Dealer Wins"
        print "Player Cards:"
        for i in playerCards:
            print cardNames[i], i+2  
        print "Dealer Cards:"    
        for i in dealerCards:
            print cardNames[i], i+2                      
        dealerWins+=1
        sleep(2)
        deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,[],[])        
    if totalDealerAmount < totalPlayerAmount:
        print "Player Wins" 
        print "Player Cards:"
        for i in playerCards:
            print cardNames[i], i+2
        print "Dealer Cards:"    
        for i in dealerCards:
            print cardNames[i], i+2        
        playerWins+=1
        sleep(2)
        deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,[],[])                     
clear()

dealerCards = []
playerCards = []
dealerSuit = 0
playerSuit = 0
playerWins = 0
dealerWins = 0
playerDealerTies = 0
cardNames = ["2","3","4","5","6","7","8","9","King","Ace"]
suits = ["Clubs","Diamonds","Hearts","Spades"]

while 1:
    deal(playerWins,dealerWins,playerDealerTies,cardNames,suits,dealerCards,playerCards)