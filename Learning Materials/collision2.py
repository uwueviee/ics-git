""" 
A demo with multiple sprites, groups, and collisions
"""
import random
import pygame
pygame.mixer.pre_init(44100, -16, 2, 1024)
from pygame.locals import * 
pygame.init()
#when checking for collision, check direction as well.  Also, make sure directions are correct.
class Ball(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #construct the parent component
        self.image = pygame.image.load("small_ball.png").convert_alpha()
        #self.image = pygame.image.load("player_u0.png").convert_alpha()
        self.rect = self.image.get_rect() #loads the rect from the image

        #set the position, direction, and speed of the ball
        self.rect.left = random.randrange(0,screen.get_width()-self.rect.width)
        self.rect.top = random.randrange(0,screen.get_height()-self.rect.height)
        self.dir_x = random.choice([-1,1])
        self.dir_y = random.choice([-1,1])
        self.speed = random.randrange(1,4)


    def update(self, wallgroup):
        #this approach is to check - if one more move is made, will we THEN
        #be in collision?  if so, then change direction now....Here, we first
        #check in the x direction, then in the y direction
        check = self.rect.move(self.speed*self.dir_x, 0)
        print check 
        for wall in wallgroup:
            if wall.rect.colliderect(check):
                self.dir_x *= -1
                bounce.play()
                break
        else:    #don't need the else, but its a good example of this structure   
            for cball in ball_group:  #is ball colliding with another ball?
                if cball!=self:     #exclude itself from ball group
                    if check.colliderect(cball.rect):
                        self.dir_x *= -1
                        cball.dir_x *= -1
                        bounce.play()
                        break


        check = self.rect.move(0, self.speed*self.dir_y)
        for wall in wallgroup:
            if wall.rect.colliderect(check):
                self.dir_y *= -1
                bounce.play()
                break
        else:
            for cball in ball_group:
                if cball!=self:
                    if check.colliderect(cball.rect):
                        self.dir_y *= -1
                        cball.dir_y *= -1
                        bounce.play()
                        break
        
##        #Handle the walls by changing direction(s)
        if self.rect.left < 0 or self.rect.right >= screen.get_width()  :
           self.dir_x *= -1
        if self.rect.top < 0 or self.rect.bottom >= screen.get_height() :
            self.dir_y *= -1
        self.rect.move_ip(self.speed*self.dir_x, self.speed*self.dir_y) 

    def adjust_speed(self, delta):      #change ball speed by delta sent as argument
        self.speed += delta
        
class Block(pygame.sprite.Sprite):
    def __init__(self, colour, y, x):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((10,10)).convert()
        self.image.fill(colour)
        self.rect = self.image.get_rect()

        #set the position of the block
        self.rect.left = x
        self.rect.top = y

        
class Wall(pygame.sprite.Sprite):
    def __init__(self, colour, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50,50)).convert()
        self.image.fill(colour)
        self.rect = self.image.get_rect()

        #set the position of the block
        self.rect.left = x
        self.rect.top = y
        
#Start of game
screen = pygame.display.set_mode((640, 480))

#make some blocks and a separate group for them
block_group = pygame.sprite.Group()
for i in range(18):     #vertical group on the left
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, 20 +i * 25, 100))  #arguments are colour, y, x
for j in range(21):     #horizontal group
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, 70, j * 25 + 100))
for i in range(18):     #vertical group on the right/middle
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, i * 25, 200))

wall_group = pygame.sprite.Group()
wall1 = Wall((255, 255, 0), 290, 200)
wall_group.add(wall1)
wall2 = Wall((255, 0, 0), 450, 100)
wall_group.add(wall2)
wall3 = Wall((255, 0, 0), 350, 400)
wall_group.add(wall3)

#make the ball(s) and a group for them
ball_group = pygame.sprite.Group()
 
while len(ball_group)<10:
    ball = Ball()
    #don't add it to the group if its in collision with another ball or any wall
    if not (pygame.sprite.spritecollideany(ball, wall_group) or \
            pygame.sprite.spritecollideany(ball, ball_group)):
    #following is another approach
    #if not any([pygame.sprite.spritecollideany(ball, group) for group in [wall_group, ball_group]]):
        ball_group.add(ball)        #OK to add the ball now
     
bounce = pygame.mixer.Sound("ball_bounce.ogg") #used for a sound effect

pygame.mixer.music.load("bg_music.mp3") #used for music files
pygame.mixer.music.set_volume(0.4)
pygame.mixer.music.play(-1)
    
background = pygame.Surface(screen.get_size()).convert()
background.fill((255, 255, 255))
screen.blit(background, (0,0))

clock = pygame.time.Clock()
keep_going = True
while keep_going:
    clock.tick(100)
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
        elif ev.type == KEYDOWN:
            #Ability to increase/decrease speed
            if ev.key == K_UP:
                for ball in ball_group.sprites():
                    ball.adjust_speed(1)
            elif ev.key == K_DOWN:
                for ball in ball_group.sprites():
                    ball.adjust_speed(-1)
            elif ev.key == K_SPACE:
                 pygame.time.delay(5000)
            
    #for every ball check for collisions, killing the blocks hit
    for ball in ball_group.sprites():
        hits = pygame.sprite.spritecollide(ball, block_group, True)

    #handle when the blocks are all gone
    if len(block_group) == 0:
        keep_going = False

    wall_group.clear(screen, background)
    ball_group.clear(screen, background)
    block_group.clear(screen, background)
    ball_group.update(wall_group.sprites())
    block_group.draw(screen)
    wall_group.draw(screen)
    ball_group.draw(screen)
    pygame.display.flip()
pygame.quit()    
