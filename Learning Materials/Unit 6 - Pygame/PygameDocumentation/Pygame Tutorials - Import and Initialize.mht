From: <Saved by Windows Internet Explorer 8>
Subject: Pygame Tutorials - Import and Initialize
Date: Wed, 30 Nov 2011 11:23:38 -0500
MIME-Version: 1.0
Content-Type: text/html;
	charset="utf-8"
Content-Transfer-Encoding: quoted-printable
Content-Location: http://www.pygame.org/docs/tut/ImportInit.html
X-MimeOLE: Produced By Microsoft MimeOLE V6.00.2900.5994

=EF=BB=BF<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--=0A=
TUTORIAL:Import and Initialize=0A=
--><HTML><HEAD><TITLE>Pygame Tutorials - Import and Initialize</TITLE>
<META content=3D"text/html; charset=3Dutf-8" http-equiv=3DContent-Type>
<META name=3DGENERATOR content=3D"MSHTML 8.00.6001.19019"></HEAD>
<BODY>
<H1 align=3Dcenter><FONT size=3D-1>Pygame Tutorials</FONT><BR>Import and =

Initialize</H1>
<H2 align=3Dcenter>by Pete Shinners<BR><FONT =
size=3D-1>pete@shinners.org</FONT></H2>
<H3 align=3Dcenter>Revision 1.0, January 28th, 2002</H3><BR><BR>Getting =
pygame=20
imported and initialized is a very simple process. It is also flexible =
enough to=20
give you control over what is happening. Pygame is a collection of =
different=20
modules in a single python package. Some of the modules are written in =
C, and=20
some are written in python. Some modules are also optional, and might =
not always=20
be present. <BR>&nbsp;<BR>This is just a quick introduction on what is =
going on=20
when you import pygame. For a clearer explanation definitely see the =
pygame=20
examples. <BR>&nbsp;<BR>&nbsp;<BR>
<H2>Import</H2>First we must import the pygame package. Since pygame =
version 1.4=20
this has been updated to be much easier. Most games will import all of =
pygame=20
like this.=20
<UL><PRE>import pygame
from pygame.locals import *</PRE></UL>The first line here is the only =
necessary=20
one. It imports all the available pygame modules into the pygame =
package. The=20
second line is optional, and puts a limited set of constants and =
functions into=20
the global namespace of your script. <BR>&nbsp;<BR>An important thing to =
keep in=20
mind is that several pygame modules are optional. For example, one of =
these is=20
the font module. When you "import pygame", pygame will check to see if =
the font=20
module is available. If the font module is available it will be imported =
as=20
"pygame.font". If the module is not available, "pygame.font" will be set =
to=20
None. This makes it fairly easy to later on test if the font module is=20
available. <BR>&nbsp;<BR>&nbsp;<BR>
<H2>Init</H2>Before you can do much with pygame, you will need to =
initialize it.=20
The most common way to do this is just make one call.=20
<UL><PRE>pygame.init()</PRE></UL>This will attempt to initialize all the =
pygame=20
modules for you. Not all pygame modules need to be initialized, but this =
will=20
automatically initialize the ones that do. You can also easily =
initialize each=20
pygame module by hand. For example to only initialize the font module =
you would=20
just call.=20
<UL><PRE>pygame.font.init()</PRE></UL>Note that if there is an error =
when you=20
initialize with "pygame.init()", it will silently fail. When hand =
initializing=20
modules like this, any errors will raise an exception. Any modules that =
must be=20
initialized also have a "get_init()" function, which will return true if =
the=20
module has been initialized. <BR>&nbsp;<BR>It is safe to call the init() =

function for any module more than once. <BR>&nbsp;<BR>&nbsp;<BR>
<H2>Quit</H2>Modules that are initialized also usually have a quit() =
function=20
that will clean up. There is no need to explicitly call these, as pygame =
will=20
cleanly quit all the initilized modules when python finishes. =
</BODY></HTML>
