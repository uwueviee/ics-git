""" 
Testing out Fonts
"""
import pygame
from pygame.locals import * 
pygame.init()

screen = pygame.display.set_mode((400, 400))
background = pygame.Surface(screen.get_size()).convert()
background.fill((255, 255, 255))

clock = pygame.time.Clock()
keep_going = True
while keep_going:
    clock.tick(30)
    


    pygame.draw.circle(background,(0,0,44),(34,112),40)
    pygame.draw.circle(background,(200,0,40),(164,52),60)
    screen.blit(background, (0,0))
    pygame.display.flip()
    pygame.time.delay(400)
    pygame.draw.circle(background,(200,0,44),(34,112),40)
    pygame.draw.circle(background,(0,0,40),(164,52),60)
    screen.blit(background, (0,0))
    pygame.display.flip()
    pygame.time.delay(400)

    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
pygame.quit()
