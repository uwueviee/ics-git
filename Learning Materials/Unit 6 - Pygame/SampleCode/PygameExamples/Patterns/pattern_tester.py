from patterns import * #all students need to do to get access to your code

#Good situation to emphasize constants, or could use input values
WIDTH = 600
HEIGHT = 600

#Create the pattern object
pat = Pattern(WIDTH, HEIGHT)

#We will need a second list as a buffer to store the next generation while we
#are filling it up, otherwise we will be comparing 'new' neighbour states
#instead of the previous states.

cur_row = [] #The current generation
new_row = [] #The next generation

#Fill our current and 'buffer' row with default values
for i in range(WIDTH):
    cur_row.append(False)
    new_row.append(False)

#Make some initial markings (could hard-code, or allow user input)
done = False
while not done:
    pos = raw_input("Enter a row index to start as True (q to quit): ")
    if pos.lower() == 'q':
	done = True
    elif not pos.isdigit() or int(pos) > WIDTH-1:
	print "Invalid index."
    else:
	cur_row[int(pos)] = True

#Add this first row
pat.add_row(cur_row)


#Since we have added a row already, go one less generation
for gen in range(HEIGHT-1):
    
    #Handle every element in the row except first & last
    for i in range(1, WIDTH-1):
	#iff an element's neighbours are not the same, then the element will be True in next gen
	if cur_row[i-1] != cur_row[i+1]:
	    new_row[i] = True
	else:
	    new_row[i] = False

    #Handle the first element in the row - only 1 neighbour
    if cur_row[1] == True:
	new_row[0] = True
    else:
	new_row[0] = False
	
    #Handle the last element in the row - only 1 neighbour
    if cur_row[-2] == True:
	new_row[-1] = True
    else:
	new_row[-1] = False

    cur_row = new_row[:] #need to assign to a copy of the list, or both will refer to same list
    pat.add_row(cur_row) #update our pattern

#Allow user to save the pattern as an image file for closer inspection
save = raw_input("Do you wish to save the image? (y/n): ")
if save.lower() == "y":
    pat.save(raw_input("Enter a file name, including .jpg/.bmp/.png: "))
    


