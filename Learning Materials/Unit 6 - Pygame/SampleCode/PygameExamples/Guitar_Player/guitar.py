import pygame
import sys
from pygame.locals import * 
pygame.mixer.pre_init(44100, -16, 2, 1024)
pygame.init()

#Joystick Code
pygame.joystick.init()
try:
    j = pygame.joystick.Joystick(0) # create a joystick instance
    j.init() # init instance
except pygame.error:
    print 'no joystick found.'
    sys.exit(1)
    

    
size = (640, 480)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Play Guitar!")


background = pygame.Surface(screen.get_size())
background = background.convert()
background.fill((0, 0, 0)) 

myFont = pygame.font.SysFont("Comic Sans MS", 30)
label = myFont.render("Press a button and strum!", True, (255,255,255))

#Load Sounds
em_chord = pygame.mixer.Sound("Em.ogg")
em_chord.set_volume(0.5)
f_chord = pygame.mixer.Sound("F.ogg")
f_chord.set_volume(0.5)
g_chord = pygame.mixer.Sound("G.ogg")
g_chord.set_volume(0.4)
am_chord = pygame.mixer.Sound("Am.ogg")
am_chord.set_volume(0.4)
c_chord = pygame.mixer.Sound("C.ogg")
c_chord.set_volume(0.7)
d_chord = pygame.mixer.Sound("D.ogg")
d_chord.set_volume(0.7)


#status of buttons: True means on
GREEN = 0
RED = 1
YELLOW = 2
BLUE = 3
ORANGE = 4
strum_status = False
button_reset = [False, False, False, False, False] #makes resetting easier later
button_status = button_reset[:]


clock = pygame.time.Clock()
keepGoing = True
while keepGoing:
    clock.tick(20)
    
    for ev in pygame.event.get():
	if ev.type == QUIT:
	    keepGoing = False
	#HANDLE STRUMMING
	elif ev.type == JOYHATMOTION: # 9
	    print ev
	    if ev.value == (0, 0) :
		strum_status = False
	    else:
		strum_status = True

	#HANDLE BUTONS ON	
	elif ev.type == JOYBUTTONDOWN: # 10
	    if ev.button == 0:
		button_status = button_reset[:]
		button_status[GREEN] = True
	    elif ev.button == 1:
		button_status = button_reset[:]
		button_status[RED] = True
	    elif ev.button == 2:
		button_status = button_reset[:]
		button_status[BLUE] = True
	    elif ev.button == 3:
		button_status = button_reset[:]
		button_status[YELLOW] = True
	    elif ev.button == 4:
		button_status = button_reset[:]
		button_status[ORANGE] = True
	#HANDLE BUTTONS OFF	
	elif ev.type == JOYBUTTONUP: # 10
	    button_status = button_reset[:]

    #Play the right sound
    if button_status[GREEN] and strum_status:
	pygame.mixer.stop()
	em_chord.play()	    
	strum_status = False
    elif button_status[RED] and strum_status:
	pygame.mixer.stop()
	g_chord.play()
	strum_status = False
    elif button_status[YELLOW] and strum_status:
	pygame.mixer.stop()
	am_chord.play()
	strum_status = False
    elif button_status[BLUE] and strum_status:
	pygame.mixer.stop()
	c_chord.play()
	strum_status = False
    elif button_status[ORANGE] and strum_status:
	pygame.mixer.stop()
	d_chord.play()
	strum_status = False
