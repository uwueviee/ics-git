""" 
A basic program establishing a simple framework for future games.
"""

#Import & initialize the pygame module
import pygame
from pygame.locals import *  #This is not necessary for most games, but can simplify some things
pygame.init()



#Set-up the main display window and the background
size = (640, 480)  #<-- that is a tuple (just like a list) for width & height
screen = pygame.display.set_mode(size) #<-- screen is now a Surface type object
pygame.display.set_caption("Yeah, Pygame!") #<-- caption appears in the title bar


background = pygame.Surface(size) #<-- like display, but creates a Surface object from scratch
background = background.convert() #<-- creates a copy of the Surface with a standard (faster) 
                                  #    colour format
background.fill((0, 0, 255)) #<-- fills Surface with colour using a tuple (red, green, blue).
                             #    See http://en.wikipedia.org/wiki/RGB_color_model for RGB info
                             #    See http://en.wikipedia.org/wiki/List_of_colors for colours


#The game loop
clock = pygame.time.Clock() #<-- used to control the frame rate
keep_going = True 	        #<-- a 'flag' variable for the game loop condition

while keep_going:

    clock.tick(30) #<-- Set a constant frame rate, argument is frames per second
        
    #Handle any events in the current frame
    for ev in pygame.event.get(): #<-- returns a list of all Events in this frame
                                  #    and this loops over each event, acting accordingly

        #Events are objects with a type instance variable (an int, linked to pygame constants).
        #These types could be a certain key pressed, a mouse moved, or even a guitar strum!
        #These event types are mapped to constants in the pygame class.
        #You can see them all with help(pygame).

        if ev.type == QUIT: #<-- this special event type happens when the window is closed
            keep_going = False
        elif ev.type == KEYDOWN: #<-- if it was a keyboard press
            if ev.key == K_r:    #<-- keyboard events have a key property
                background.fill((255, 0, 0)) #<-- fill the background red
                pygame.display.set_caption("RED!") #<-- update the caption    
            elif ev.key == K_g:    
                background.fill((0, 255, 0))
                pygame.display.set_caption("GREEN!")
            elif ev.key == K_b:    
                background.fill((0, 0, 255))
                pygame.display.set_caption("BLUE!")    
            else:
                key_label = ev.unicode #<-- gets the unicode property value from the event
                pygame.display.set_caption(str(key_label)*10) #<-- update the caption    

    #Update and refresh the display to end this frame
    screen.blit(background, (0, 0)) #<-- 'blit' means to copy one Surface to another
                                    #    Here, we copy the background onto the screen Surface
    pygame.display.flip() #<-- refresh the display
pygame.quit()
