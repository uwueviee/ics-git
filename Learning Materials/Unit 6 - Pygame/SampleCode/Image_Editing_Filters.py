""" 
Basic editing of an image through an algorithm
"""
import pygame
from pygame.locals import * 
pygame.init()

img = pygame.image.load("lorikeet.bmp")
screen = pygame.display.set_mode(img.get_size())
pygame.display.set_caption("Image Modifying")
img = img.convert()


#MODIFY IMAGE CODE GOES HERE.  UNCOMMENT ONE SECTION TO SEE THE EFFECT.
#Take the negative of the image
for row in range(img.get_height()):    
    for col in range(img.get_width()):
        pix = img.get_at((col,row))
        new_pix = (255-pix[0], 255-pix[1], 255-pix[2])
        img.set_at((col, row), new_pix)

"""
#Flip the image across a vertical mid-line
for row in range(img.get_height()):  
    width = img.get_width()
    for col in range(width/2):
        r_pix = img.get_at((width-col-1, img.get_height()-row-1))
        l_pix = img.get_at((col,row))
        img.set_at((col, row), r_pix)
        img.set_at((width-col-1, img.get_height()-row-1), l_pix)
"""

"""
#A sepia filter
for row in range(img.get_height()):
    for col in range(img.get_width()):
        pix = img.get_at((col,row))
        avg = (pix[0] + pix[1] + pix[2])/3
        pix = [avg, avg, avg]
        if pix[0] < 63:
            pix[0] = int(pix[0]*1.1)
            pix[2] = int(pix[2]*.9)
        elif pix[0] < 192:
            pix[0] = int(pix[0]*1.15)
            pix[2] = int(pix[2]*.85)
        else:
            pix[0] = min( int(pix[0]*1.08), 255)
            pix[2] = int(pix[2]*.93)
        img.set_at((col, row), pix)
"""

screen.blit(img, (0,0))
pygame.display.flip()
keep_going = True
while keep_going:
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
            