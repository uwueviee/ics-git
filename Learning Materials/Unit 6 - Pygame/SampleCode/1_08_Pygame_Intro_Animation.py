""" 
Basic animation/movement of an image Surface
"""
import pygame
from pygame.locals import * 
pygame.init()

screen = pygame.display.set_mode((640, 480))
img = pygame.image.load("lorikeet.bmp").convert()
img_rect = img.get_rect()

background = pygame.Surface(screen.get_size()).convert()
background.fill((255, 255, 255))
screen.blit(background, (0,0))  #place the background onto the screen surface
screen.blit(img, img_rect)

#clock = pygame.time.Clock()
#print clock
keep_going = True
while keep_going:
    pygame.time.delay(30)
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
        elif ev.type == MOUSEBUTTONDOWN:
            print
    img_rect.top += 1
    img_rect.left += 1
    #img_rect = img_rect.move(1,1) #ALSO WORKS
    #img_rect.move_ip(1,1) #ALSO WORKS

    screen.blit(background, (0,0)) #UNCOMMENT THIS TO ELIMINATE 'STREAKS'
    screen.blit(img, img_rect)
    pygame.display.flip()
pygame.quit()

