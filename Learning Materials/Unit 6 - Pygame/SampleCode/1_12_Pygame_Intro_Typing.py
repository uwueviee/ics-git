""" 
Text input on a Surface
"""
import pygame
from pygame.locals import * 
pygame.init()

screen = pygame.display.set_mode((400, 100))
background = pygame.Surface(screen.get_size()).convert()
background.fill((0, 0, 0)) 

field_surf = pygame.Surface((300, 50)).convert()
field_surf.fill((255,255,255))

my_font = pygame.font.SysFont("helvetica", 20)
field_value = ""
field = my_font.render(field_value, True, (0,0,0))

clock = pygame.time.Clock()
keepGoing = True
while keepGoing:
    clock.tick(30)    
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keepGoing = False
        elif ev.type == KEYDOWN:
            if ev.key == K_BACKSPACE and len(field_value) > 0:
                field_value = field_value[:-1] #cut off last character
            elif (ev.unicode.isalnum() or ev.key==K_SPACE) and len(field_value) < 20:
                field_value += ev.unicode #adds character value of key
            field = my_font.render(field_value, True, (0,0,0))

    screen.blit(background, (0, 0))
    screen.blit(field_surf, (50, 25))
    screen.blit(field, (60, 40))
    pygame.display.flip()
pygame.quit()
