""" 
Respond to keyboard without events
"""
import pygame
from pygame.locals import * 
pygame.init()

size = (640, 480)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Yeah, Pygame!")

background = pygame.Surface(size)
background = background.convert()
background.fill((0, 0, 255))

clock = pygame.time.Clock()
keep_going = True

while keep_going:
    clock.tick(30)
    pygame.event.pump() #need to call once per loop when not using event handling 
    keys = pygame.key.get_pressed()
    if keys[K_ESCAPE]:
        keep_going = False
    if keys[K_r]:
        background.fill((255, 0, 0))
        pygame.display.set_caption("RED!")    
    elif keys[K_g]:
        background.fill((0, 255, 0))
        pygame.display.set_caption("GREEN!")
    elif keys[K_b]:
        background.fill((0, 0, 255))
        pygame.display.set_caption("BLUE!")    

    screen.blit(background, (0, 0))
    pygame.display.flip()
    
#Fade the background colour to black once game loop is done
colour = background.get_at( (0,0) ) #get the colour on the background Surface
while colour != (0,0,0):
    #decrease the r/g/b  by 1, but dont allow them to go below 0
    colour = (max(colour[0]-1,0), max(colour[1]-1,0), max(colour[2]-1,0))
    background.fill(colour) #refill the background with the new colour
    screen.blit(background, (0, 0))
    pygame.display.flip()
pygame.quit()
