""" 
Basic editing of an image through mouse clicks
"""
import pygame
from pygame.locals import * 
pygame.init()

img = pygame.image.load("lorikeet.bmp") #load an image as a Surface

screen = pygame.display.set_mode(img.get_size()) #using image's size to determine the window size
pygame.display.set_caption("Drag the mouse to draw!")

img = img.convert() #need to convert it after we have set-up the screen

#the display will not change, so we can blit & flip the display just once first
screen.blit(img, (0,0))
pygame.display.flip()


clock = pygame.time.Clock()
keep_going = True

prev_pos = (-1,-1)
while keep_going:
    clock.tick(30)
        
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
    if pygame.mouse.get_pressed()[0]: #if left mouse is pressed
        if prev_pos == (-1,-1):
            #first point
            prev_pos = pygame.mouse.get_pos()
        else:
            new_pos = pygame.mouse.get_pos()
            pygame.draw.line(img, (255,255,255), prev_pos, new_pos) #See the 'draw' module
            prev_pos = new_pos
    else:
        prev_pos = (-1,-1)
    screen.blit(img, (0,0))
    pygame.display.flip()
pygame.quit()
            
