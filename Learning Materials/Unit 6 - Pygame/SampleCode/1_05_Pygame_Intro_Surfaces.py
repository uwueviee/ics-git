""" 
Exploring basic Surfaces and colours
"""
import pygame
from pygame.locals import * 
pygame.init()

full_size = (640, 480) #full screeen size
bar_size = (640, 80)   #size of each colour bar
screen = pygame.display.set_mode(full_size)

background = pygame.Surface(full_size).convert()
bg_colour = (0, 0, 0) #it helps to have the colour as a separate variable
background.fill(bg_colour)
pygame.display.set_caption("Colour: " + str(bg_colour)) #our caption will change

#Create the three colour bars
red_bar = pygame.Surface(bar_size).convert()
red_bar.fill((255, 0, 0))
green_bar = pygame.Surface(bar_size).convert()
green_bar.fill((0, 255, 0))
blue_bar = pygame.Surface(bar_size).convert()
blue_bar.fill((0, 0, 255))

clock = pygame.time.Clock()
keep_going = True

while keep_going:
    clock.tick(30)
        
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
        elif ev.type == MOUSEBUTTONDOWN: #A mouse click!
            x = ev.pos[0]  #the MOUSEBUTTONDOWN event has a position property
            y = ev.pos[1]  #that is an (x, y) tuple
            
            #Determine the bar that was clicked by checking the y-coordinate
            if y >= 0 and y < 80:   #we hit the red bar
                #Convert the x-coord to a % of the width, used for amount of colour
                #then adjust that element of the bg_colour, keeping the other two.
                red = (1.0*x/(bar_size[0]-1))*255
                bg_colour = (int(red+.5), bg_colour[1], bg_colour[2])
            elif y >= 80 and y < 160:   #we hit the green bar
                green = (1.0*x/(bar_size[0]-1))*255
                bg_colour = (bg_colour[0], int(green+.5), bg_colour[2])
            elif y >= 160 and y < 240:   #we hit the blue bar
                blue = (1.0*x/(bar_size[0]-1))*255
                bg_colour = (bg_colour[0], bg_colour[1], int(blue+.5))

            #now update the background and caption
            background.fill(bg_colour)
            pygame.display.set_caption("Colour: " + str(bg_colour))
            
    #Here we blit multiple surfaces. Order is important if overlapping.
    screen.blit(background, (0, 0))
    screen.blit(red_bar, (0, 0))
    screen.blit(green_bar, (0, 80))  #Note how we have to adjust the top-right corner
    screen.blit(blue_bar, (0, 160))
    pygame.display.flip()
pygame.quit()
    
