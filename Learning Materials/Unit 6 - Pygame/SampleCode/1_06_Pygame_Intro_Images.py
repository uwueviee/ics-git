""" 
Basic image loading as a Surface, and mouse events
"""
import pygame
from pygame.locals import * 
pygame.init()

img = pygame.image.load("lorikeet.bmp") #load an image as a Surface

screen = pygame.display.set_mode(img.get_size()) #using image's size to determine the window size
pygame.display.set_caption("Point at pixels to check their colour!")
print screen
#img = img.convert() #need to convert it after we have set-up the screen

#the display will not change, so we can blit & flip the display just once first
screen.blit(img, (0,0))
pygame.display.flip()


clock = pygame.time.Clock()
keep_going = True
 
while keep_going:
    clock.tick(30)
        
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
        elif ev.type == MOUSEMOTION:  #moving the mouse!
            pixel = img.get_at(ev.pos)[:3] #just takes (r,g,b), ignores alpha
            pygame.display.set_caption("Pixel Colour: " + str(pixel))
        elif ev.type == MOUSEBUTTONDOWN:
            img.set_at(ev.pos, (255,255,255)) #sets clicked pixels to white
            screen.blit(img, (0,0))
            pygame.display.flip()
pygame.quit()
    
