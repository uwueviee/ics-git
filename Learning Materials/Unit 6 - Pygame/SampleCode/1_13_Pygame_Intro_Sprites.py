""" 
Basic Sprite version of our ball bouncer
"""
import pygame
from pygame.locals import * 
pygame.init()

class Ball(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #construct the parent component
        self.image = pygame.image.load("ball.bmp").convert()
        self.rect = self.image.get_rect() #loads the rect from the image

        #set the position, direction, and speed of the ball
        self.rect.topleft = (10, 10)
        self.dir_x = 1
        self.dir_y = 1
        self.speed = 4	

    def update(self):
        #Handle the walls by changing direction(s)
        if self.rect.left < 0 or self.rect.right >= screen.get_width():
            self.dir_x *= -1
        if self.rect.top < 0 or self.rect.bottom >= screen.get_height():
            self.dir_y *= -1
        self.rect.move_ip(self.speed*self.dir_x, self.speed*self.dir_y)

    def adjust_speed(self, delta):
        self.speed += delta
        
screen = pygame.display.set_mode((640, 480))
ball = Ball()

background = pygame.Surface(screen.get_size()).convert()
background.fill((255, 255, 255))

clock = pygame.time.Clock()
keep_going = True
while keep_going:
    clock.tick(30)
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False
        elif ev.type == KEYDOWN:
            #Ability to increase/decrease speed
            if ev.key == K_UP:
                ball.adjust_speed(1)
            elif ev.key == K_DOWN:
                ball.adjust_speed(-1)
        
    ball.update()
    screen.blit(background, (0,0))
    screen.blit(ball.image, ball.rect)
    pygame.display.flip()
pygame.quit()    