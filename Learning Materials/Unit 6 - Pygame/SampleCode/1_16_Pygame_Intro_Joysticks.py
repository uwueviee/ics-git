import pygame
from pygame import locals
pygame.init()

#Set-up the joystick(s) - loop only needed is multiple
pygame.joystick.init()
for i in range(pygame.joystick.get_count()):
    try:
	j = pygame.joystick.Joystick(i) # create a joystick instance
	j.init() # init instance
	print 'Enabled joystick: ' + j.get_name()
    except pygame.error:
	print 'No joystick #%i found!  Please fix this!' %(i)

keepGoing = True
clock = pygame.time.Clock()

if pygame.joystick.get_count() == 0:
    print "You need a joystick for this demo!"
    keepGoing = False

#Game Loop
while keepGoing:
    clock.tick(30)
    
    #  Joystick Event Handling
    for ev in pygame.event.get():
	if ev.type == pygame.locals.JOYAXISMOTION: #a LOT of these!
	    print "Axis #%i on joystick id#%i -->%s" %(ev.axis, ev.joy, ev.value)
	elif ev.type == pygame.locals.JOYHATMOTION: #The up/down/left/right
	    print "Presssed hat#%i on joystick id#%i -->%s" %(ev.hat, ev.joy, ev.value)
	    #The value tuples are (x,y), with -1 meaning left/down, 1 meaning right/up
	elif ev.type == pygame.locals.JOYBUTTONDOWN: #any button pressed
 	    print "Pressed button #%i on joystick id#%i" %(ev.button, ev.joy)
	elif ev.type == pygame.locals.JOYBUTTONUP: #any button released
 	    print "Released button #%i on joystick id#%i" %(ev.button, ev.joy)
	    
	    
	    