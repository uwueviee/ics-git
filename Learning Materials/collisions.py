""" 
A demo with multiple sprites, groups, and collisions

-try to add speed adjustment, randomize placement of blocks and have ball
bounce in proper direction.
Then add walls and balls after certain amounts of time.  
"""
import random
import pygame
from pygame.locals import * 
pygame.init()

class Ball(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #construct the parent component
        self.image = pygame.image.load("small_ball.png").convert_alpha()
        self.rect = self.image.get_rect() #loads the rect from the image

        #set the position, direction, and speed of the ball
        self.rect.left = random.randrange(0,screen.get_width()-self.rect.width)
        self.rect.top = random.randrange(0,screen.get_height()-self.rect.height)
        self.dir_x = random.choice([-1,1])
        self.dir_y = random.choice([-1,1])
        self.speed = random.randrange(1,4)


    def update(self, aWall):
        #Handle the walls by changing direction(s)
        if self.rect.left < 0 or self.rect.right >= screen.get_width() or aWall == True:
            self.dir_x *= -1
        if self.rect.top < 0 or self.rect.bottom >= screen.get_height() or aWall == True:
            self.dir_y *= -1
        self.rect.move_ip(self.speed*self.dir_x, self.speed*self.dir_y) 

        
class Block(pygame.sprite.Sprite):
    def __init__(self, colour, y, x):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((10,10)).convert()
        self.image.fill(colour)
        self.rect = self.image.get_rect()

        #set the position of the block
        self.rect.left = x
        self.rect.top = y

        
class Wall(pygame.sprite.Sprite):
    def __init__(self, colour, x, y):       #colours,x,and y passed in from call
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50,50)).convert()
        self.image.fill(colour)
        self.rect = self.image.get_rect()

        #set the position of the block
        self.rect.left = x
        self.rect.top = y


        
#Start of game
screen = pygame.display.set_mode((640, 480))

#make the ball(s) and a group for them
ball_group = pygame.sprite.Group()
for i in range(1):
    ball = Ball()
    ball_group.add(ball)

#make some blocks and a separate group for them
block_group = pygame.sprite.Group()
for i in range(10):
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, 100 +i * 25, 100))
for j in range(4):
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, 100, j * 25 + 100))
for i in range(20):
    block_colour = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
    block_group.add(Block(block_colour, i * 25, 200))

wall_group = pygame.sprite.Group()
wall1 = Wall((255, 255, 0), 290, 200)
wall_group.add(wall1)
wall2 = Wall((255, 0, 0), 450, 100)
wall_group.add(wall2)
wall3 = Wall((255, 0, 0), 350, 400)
wall_group.add(wall3)

    
background = pygame.Surface(screen.get_size()).convert()
background.fill((255, 255, 255))
screen.blit(background, (0,0))

clock = pygame.time.Clock()
keep_going = True
while keep_going:
    clock.tick(30)
    for ev in pygame.event.get():
        if ev.type == QUIT:
            keep_going = False

            
    #for every ball check for collisions, killing the blocks hit
    for ball in ball_group.sprites():
        hits = pygame.sprite.spritecollide(ball, block_group, True)


    #some_rect.colliderect(other_rect)

    for wall in wall_group.sprites():
        if wall.rect.colliderect(ball) :
            ball_group.update(True)     #we're in collision with walls, so change direction in ball.update function
            
    
    #handle when the blocks are all gone
    if len(block_group) == 0:
        keep_going = False

    wall_group.clear(screen, background)
    ball_group.clear(screen, background)
    block_group.clear(screen, background)
    ball_group.update(False)
    block_group.draw(screen)
    wall_group.draw(screen)
    ball_group.draw(screen)
    pygame.display.flip()
pygame.quit()    
