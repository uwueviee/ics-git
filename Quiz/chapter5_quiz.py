# Alcide Viau
userInput = 1
weights = []
while 1:
    userInput = input("Enter the weight of the pumpkin > ")
    # This causes the loop to break without adding the extra digit when entering 0 (or below)
    if userInput <= 0:
        break
    else:
        weights.append(userInput)
for i in weights:
    # Gets the indexed value and compares it
    if i >= 5 and i <= 10:
        print i,"Grocery Store"
    elif i >= 11 and i <= 100:
        print i,"Jack-o-lantern"
    elif i >= 101 and i <= 500:
        print i,"County Fair"
    elif i >= 501:
        print i,"Guiness Book Contender"
    elif i < 5:
        print i,"Leave them on the field"        